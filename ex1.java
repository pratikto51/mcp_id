import java.util.Scanner;

public class ex1{
    public static void main(String args[]){

        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Jumlah angka : ");
        int n = input.nextInt();

        int data[] = new int[n];

        for(int u = 0; u < n; u++){
            System.out.print("masukan angka ke-"+(u+1)+" : ");
            data[u] = input.nextInt();
        }

        boolean checker;

        for(int u = 0; u < n; u++){
            checker = true;
            for(int v = 0; v < n; v++){
                if(data[u]-data[v] < 0){
                    checker = false;
                }
            }
            if(checker){
                System.out.print(data[u]);
            }
        }
    }
}